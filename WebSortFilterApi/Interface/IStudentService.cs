﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSortFilterApi.Models;

namespace WebSortFilterApi.Interface
{
    public interface IStudentService
    {
        List<Student> GetAllStudentList();
        List <Student> GetStudentById(int id);
        List<Student> AddStudent([FromBody]Student Stu);
        List<Student> UpdateModel([FromBody]Student Stu);
        List<Student> DeleteStudent(int id);
    }
}
