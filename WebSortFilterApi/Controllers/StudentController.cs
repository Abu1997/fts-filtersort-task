﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebSortFilterApi.Interface;
using WebSortFilterApi.Models;

namespace WebSortFilterApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        private readonly IStudentService _service;
        public StudentController(IConfiguration configuration, IStudentService service)
        {
            _configuration = configuration;

            _service = service;
        }

        [HttpGet("list")]
        public IActionResult GetList()
        {
            return Ok(_service.GetAllStudentList());
        }

        [HttpGet("single/:id")]
        public IActionResult GetSingleId(int Id)
        {
            string query = @"select StudentId,StudentName,Country
                             from Student where StudentId = " + Id + @"
                            ";

            DataTable table = new DataTable();

            string SqlDataSource = _configuration.GetConnectionString("DefaultConnection");

            SqlDataReader Reader;

            using (SqlConnection connection = new SqlConnection(SqlDataSource))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    Reader = command.ExecuteReader();
                    table.Load(Reader);


                    Reader.Close();
                    connection.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpGet("list/sorting")]

        public IActionResult Sorting(string orderByname, string ordertype)
        {
            string query = @"SELECT * FROM Student 
                            ORDER BY " + orderByname + " " + ordertype + "";


            DataTable table = new DataTable();

            string SqlDataSource = _configuration.GetConnectionString("DefaultConnection");

            SqlDataReader Reader;

            using (SqlConnection connection = new SqlConnection(SqlDataSource))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    Reader = command.ExecuteReader();
                    table.Load(Reader);


                    Reader.Close();
                    connection.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpGet("list/filter")]

        public IActionResult Filter(string filterbyValue, string filterbyName)
        {
            string query = @"select * from Student where " + filterbyValue + " like '" + filterbyName + "'";



            DataTable table = new DataTable();

            string SqlDataSource = _configuration.GetConnectionString("DefaultConnection");

            SqlDataReader Reader;

            using (SqlConnection connection = new SqlConnection(SqlDataSource))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    Reader = command.ExecuteReader();
                    table.Load(Reader);


                    Reader.Close();
                    connection.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpGet("Single/{id}")]

        public IActionResult GetId(int id)
        {
            return Ok(_service.GetStudentById(id));
        }


        [HttpPost("create")]
        public IActionResult PostList([FromBody] Student Stu)
        {
            return Ok(_service.AddStudent(Stu));
        }


        [HttpPut("update")]

        public IActionResult putstudent([FromBody] Student stu)
        {
            return Ok(_service.UpdateModel(stu));
        }

        [HttpDelete("delete/:id")]
        public IActionResult DeleteStudent([FromBody] int Id)
        {
            return Ok(_service.DeleteStudent(Id));
        }
    }
}
