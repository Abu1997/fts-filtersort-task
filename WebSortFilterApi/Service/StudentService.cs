﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebSortFilterApi.Interface;
using WebSortFilterApi.Models;

namespace WebSortFilterApi.Service
{
    public class StudentService : IStudentService
    {
        private readonly IConfiguration _configuration;

        public StudentService (IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public List<Student>  AddStudent(Student Stu)
        {
            string query = @"insert into Student values
                             ('" + Stu.StudentName + @"'
                             ,'" + Stu.Country + @"')
                            ";

            DataTable table = new DataTable();

            string SqlDataSource = _configuration.GetConnectionString("DefaultConnection");

            SqlDataReader reader;

            using (SqlConnection connection = new SqlConnection(SqlDataSource))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    reader = command.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    connection.Close();
                }
            }
            var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(table));
            return source;
        }

        public List<Student> DeleteStudent(int Id)
        {
            string query = @"delete from Student
                            where StudentId = " + Id + @"
                            ";

            DataTable table = new DataTable();

            string SqlDataSource = _configuration.GetConnectionString("DefaultConnection");

            SqlDataReader reader;

            using (SqlConnection connection = new SqlConnection(SqlDataSource))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    reader = command.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    connection.Close();
                }
            }
            var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(table));
            return source;
        }

        public List<Student> GetAllStudentList()
        {
            string query = @"select StudentId,StudentName,Country
                             from Student
                            ";

            DataTable table = new DataTable();

            string SqlDataSource = _configuration.GetConnectionString("DefaultConnection");

            SqlDataReader Reader;

            using (SqlConnection connection = new SqlConnection(SqlDataSource))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    Reader = command.ExecuteReader();
                    table.Load(Reader);


                    Reader.Close();
                    connection.Close();
                }
            }
        
            var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(table));
            return source;
        }

        public List<Student> GetStudentById(int id)
        {
            string query = @"select * from Student where StudentId = " + id + "";
                            

            DataTable table = new DataTable();

            string SqlDataSource = _configuration.GetConnectionString("DefaultConnection");

            SqlDataReader Reader;

            using (SqlConnection connection = new SqlConnection(SqlDataSource))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    Reader = command.ExecuteReader();
                    table.Load(Reader);


                    Reader.Close();
                    connection.Close();
                }
            }

            var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(table));
            return source;
        }

        public List<Student> UpdateModel(Student Stu)
        {
            string query = @"Update Student set
                             StudentName ='" + Stu.StudentName + @"'
                             ,Country ='" + Stu.Country + @"'
                              where StudentId =" + Stu.StudentId + @"
                            ";

            DataTable table = new DataTable();

            string SqlDataSource = _configuration.GetConnectionString("DefaultConnection");

            SqlDataReader reader;

            using (SqlConnection connection = new SqlConnection(SqlDataSource))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    reader = command.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    connection.Close();
                }
            }
            var source = JsonConvert.DeserializeObject<List<Student>>(JsonConvert.SerializeObject(table));
            return source;
        }

    }
}
